package de.panamo.blockchainweb;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.codec.digest.DigestUtils;
import de.panamo.blockchainweb.webhandler.GetChainHandler;
import de.panamo.blockchainweb.webhandler.MineHandler;
import de.panamo.blockchainweb.webhandler.TransactionHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;

public class BlockChain {
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private String uniqueId = UUID.randomUUID().toString().replace("-", "");
    private List<ChainBlock> blocks = new ArrayList<>();
    private List<Transaction> unMinedTransactions = new ArrayList<>();

    /**
     * Erstellt den Startblock
     */

    private BlockChain() {
        this.createBlock(100);
    }

    /**
     * Erstellt einen neuen Block.
     * @param proof Wie viele Male versucht wurde, einen Hash mit 6 Nullen am Anfang zu finden.
     * @return den neuen Block.
     */

    private ChainBlock createBlock(long proof) {
        ChainBlock chainBlock = new ChainBlock(this, proof);
        this.blocks.add(chainBlock);
        this.unMinedTransactions.clear();
        return chainBlock;
    }

    /**
     * Ermittelt den Proof für den nächsten Block.
     * Verlangsamt das minen des Blocks, da so lange ein Hash generiert wird, bis dieser mit 6 Nullen beginnt.
     * @return den Proof.
     */

    private long proofOfWork() {
        ChainBlock block = this.getLastBlock();
        long lastProof = block.getProof();
        String lastHash = block.hash();

        long proof = 0;
        while (!this.checkHash(proof, lastProof, lastHash))
            proof++;

        return proof;
    }

    /**
     * Erstellt anhand der Attribute des letzten Blocks und dem Proof einen Hash.
     * @return ob der Hash mit 6 Nullen beginnt.
     */

    private boolean checkHash(long proof, long lastProof, String lastHash) {
        //System.out.println(DigestUtils.sha256Hex(proof + lastProof + lastHash));
        return DigestUtils.sha256Hex(proof + lastProof + lastHash).startsWith("000000");
    }

    /**
     * Fügt eine neue Transaktion hinzu (Muss noch gemined werden).
     * @param transaction die Transaktion.
     */

    public void addTransaction(Transaction transaction) {
        this.unMinedTransactions.add(transaction);
        System.out.println(String.format("Es wurde eine neue Transaktion hinzugefügt. Block: %s, Sender: %s, Empfänger: %s, Betrag: %s",
                this.getLastBlock().getIndex() + 1, transaction.getSender(), transaction.getReceiver(), transaction.getAmount()));
    }

    /**
     * Mined einen neuen Block mit allen noch nicht vorhandenen Transaktionen.
     */

    public void mine() {
        long proof = this.proofOfWork();

        //Belohnung für die BlockChain für das minen das Blocks. In diesem Fall 1.
        this.addTransaction(new Transaction("0", this.uniqueId, 1));

        ChainBlock block = this.createBlock(proof);
        System.out.println(String.format("Es wurde ein neuer Block erstellt. Index: %s, Proof: %s, Hash: %s",
                block.getIndex(), block.getProof(), block.hash()));
    }

    /**
     * @return Alle Blöcke der Chain als JSON.
     */

    public String getChain() {
        return GSON.toJson(this.blocks);
    }

    /**
     * @return den letzten Block in der Chain.
     */

    public ChainBlock getLastBlock() {
        return this.blocks.get(this.blocks.size() -1);
    }

    /**
     * @return Alle Blöcke der Chain als Objekte.
     */

    public List<ChainBlock> getBlocks() {
        return blocks;
    }

    /**
     * @return Die bisher nicht verabeiteten Transaktionen.
     */

    public List<Transaction> getUnMinedTransactions() {
        return unMinedTransactions;
    }

    /**
     * Startmethode.
     * Startet den WebServer und erstellt die BlockChain.
     */

    public static void main(String[] args) throws IOException {
        int port;
        if(args.length == 1)
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException exception) {
                port = 1080;
            }
        else
            port = 1080;

        BlockChain blockChain = new BlockChain();

        /*POST (127.0.0.1:[port]/transaction) Die Transaktion muss in Form von JSON übergeben werden.
        {
            "sender":"[Sender]",
            "receiver":"[Receiver]",
            "amount":"[Amount]"
        }
        */
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/transaction", new TransactionHandler(blockChain));

        //GET (127.0.0.1:[port]/chain)
        server.createContext("/chain", new GetChainHandler(blockChain));
        //GET (127.0.0.1:[port]/mine)
        server.createContext("/mine", new MineHandler(blockChain));
        server.setExecutor(null);
        server.start();

        System.out.println("Der WebServer wurde gestartet und läuft auf 127.0.0.1:" + port);
    }

}
