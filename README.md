Beim Start des Programms kann als Argument der Port angegeben werden, auf dem der
WebServer laufen soll. Sollte keiner angegeben worden sein, wird der Port 1080 verwendet.

Eine neue Transaktion kann durch einen POST an die Adresse 127.0.0.1:[port]/transaction
hinzugefügt werden. Der Body des Posts sollte wie folgt aussehen: 

{
	"sender":"[Sender]",
	"receiver":"[Receiver]",
	"amount":"[Amount]"
}

Will man den Block mit den neusten Transaktionen nun minen, muss man einen GET an
127.0.0.1:[port]/mine durchführen.

Zum Ansehen der gesamten Chain mit allen Blöcken kann ein GET an 127.0.0.1:[port]/chain
genutzt werden.


Diese Kommunikation mit dem WebServer ist z.B mit dem Programm Postman
(https://getpostman.com) möglich.